/**
 * @file
 * Shows/hides video file storage settings depending on checked status of 
 * 'Keep a copy of the video file locally' checkbox.
 */
 
(function ($) {

  Drupal.behaviors.vimeo_video = {
    attach: function (context, settings) {
      var disableStorageOptions = function(checkbox) {
//        var elements = $('#edit-field-settings-uri-scheme input, #edit-instance-settings-file-directory');
        var elements = $('.form-item-field-settings-uri-scheme, .form-item-instance-settings-file-directory');
        if ($(checkbox).is(':checked')) {
          //elements.removeAttr('disabled');
          elements.show();
        } else {
          //elements.attr('disabled', 'disabled');
          elements.hide();
        }        
      };
      
      $('#edit-field-settings-store-local', context)
        .each(function () {
          disableStorageOptions(this);
        })
        .click(function () {
          var message = 'If this checkbox is unchecked, all video files belonging to this field will be irreversibly deleted from the server on pressing \'Save settings\' button.\nVideo at vimeo will remain untouched. Are you sure?';
          if (($(this).is(':checked'))) {
            disableStorageOptions(this);
          } else {
            if (confirm(Drupal.t(message))) {
              disableStorageOptions(this);
            } else {
              $(this).attr('checked', 'checked');
            }
          }
        });
    }
  };

})(jQuery);